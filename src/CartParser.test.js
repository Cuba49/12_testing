import CartParser from "./CartParser";

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe("CartParser - unit tests", () => {
	it('parseLine should return "name" in string format', () => {
		const product = parser.parseLine("10.10,10.10,10.10");
		expect(product.name).toEqual("10.10");
	});
	it('parseLine should return "price" and "quantity" in number format', () => {
		const product = parser.parseLine("10.10,10.10,10.10");
		expect(product.price).toEqual(10.1);
		expect(product.quantity).toEqual(10.1);
	});
	it("calcTotal should return correct total sum", () => {
		const items = [
			{
				id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				name: "Mollis consequat",
				price: 9,
				quantity: 2,
			},
			{
				id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				name: "Tvoluptatem",
				price: 10.32,
				quantity: 1,
			},
			{
				id: "33c14844-8cae-4acd-91ed-6209a6c0bc31",
				name: "Scelerisque lacinia",
				price: 18.9,
				quantity: 1,
			},
			{
				id: "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
				name: "Consectetur adipiscing",
				price: 28.72,
				quantity: 10,
			},
			{
				id: "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
				name: "Condimentum aliquet",
				price: 13.9,
				quantity: 1,
			},
		];
		expect(parser.calcTotal(items)).toEqual(348.31999999999994);
	});
	describe("validation", () => {
		it("should return error if header name is not correct", () => {
			const fileWithBrokenHeader = "Product name,price,Quantity";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.HEADER,
				0,
				1,
				`Expected header to be named "Price" but received price.`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should return error if line has less cells then need", () => {
			const fileWithBrokenHeader = "Product name,price,Quantity\n9.00,2";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.ROW,
				1,
				-1,
				`Expected row to have 3 cells but received 2.`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should return error if value in string cell is emty string", () => {
			const fileWithBrokenHeader = "Product name,price,Quantity\n,9.00,2";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				0,
				`Expected cell to be a nonempty string but received "".`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should return error if value in number cell is not number", () => {
			const fileWithBrokenHeader =
				"Product name,price,Quantity\nMollis consequat,nine,2";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				1,
				`Expected cell to be a positive number but received "nine".`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should return error if value in number cell is NaN", () => {
			const fileWithBrokenHeader =
				"Product name,price,Quantity\nMollis consequat,9.00,NaN";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				2,
				`Expected cell to be a positive number but received "NaN".`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should return error if value in number cell less then 0", () => {
			const fileWithBrokenHeader =
				"Product name,price,Quantity\nMollis consequat,9.00,-5";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				2,
				`Expected cell to be a positive number but received "-5".`
			);
			expect(errorList).toContainEqual(error);
		});
		it("should don`t return error about empty string if count cells less then need", () => {
			const fileWithBrokenHeader = "Product name,price,Quantity\n,9.00";
			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				0,
				`Expected cell to be a nonempty string but received "".`
			);
			expect(errorList).not.toContainEqual(error);
		});
		it("should don`t return error about not positive number if count cells less then need", () => {
			const fileWithBrokenHeader = "Product name,price,Quantity\n20.2,nine";

			const errorList = parser.validate(fileWithBrokenHeader);
			const error = parser.createError(
				parser.ErrorType.CELL,
				1,
				2,
				`Expected cell to be a positive number but received "nine".`
			);
			expect(errorList).not.toContainEqual(error);
		});
	});
});

describe("CartParser - integration test", () => {
		it("should throw Error if validation return error", () => {
			expect(() => {
				parser.parse("samples/cart.csv");
			}).toThrow("Validation failed!");
		});
});
